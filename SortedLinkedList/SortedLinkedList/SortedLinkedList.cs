﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;

namespace SortedLinkedList
{
    /// <summary>
    /// Отсортированный односвязный список.
    /// </summary>
    /// <typeparam name="TKey">Тип ключей.</typeparam>
    /// <typeparam name="TValue">Тип значений.</typeparam>
    [DebuggerTypeProxy(typeof(CollectionDebugView))]
    [DebuggerDisplay("Count = {Count}")]
    [Serializable]
    public partial class SortedLinkedList<TKey, TValue> : IDictionary<TKey, TValue>, IReadOnlyDictionary<TKey, TValue>, IDictionary
    {
        private int version;

        private ValueCollection values;

        private KeyCollection keys;

        private Node<TKey, TValue> Find(TKey key)
        {
            Node<TKey, TValue> current = Head;
            while (!ReferenceEquals(current, null) && (KeysComparer.Compare(key, current.Pair.Key) != 0))
                current = current.Next;
            return current;
        }

        /// <summary>
        /// Коллекция значений.
        /// </summary>
        [DebuggerTypeProxy(typeof(CollectionDebugView))]
        [DebuggerDisplay("Count = {Count}")]
        [Serializable]
        public class ValueCollection : ICollection<TValue>, ICollection
        {
            private readonly SortedLinkedList<TKey, TValue> source;

            /// <summary>
            /// Длина коллекции.
            /// </summary>
            public int Count => source.Count;

            /// <summary>
            /// Объект синхронизации.
            /// </summary>
            public object SyncRoot => source.SyncRoot;

            /// <summary>
            /// Указывает является ли исходная коллекция синхронизированной.
            /// </summary>
            public bool IsSynchronized => source.IsSynchronized;

            /// <summary>
            /// Указывает является ли коллекция коллекцией только на чтение.
            /// </summary>
            public bool IsReadOnly => true;

            /// <summary>
            /// Конструирует коллекцию значений из отсортированного списка.
            /// </summary>
            /// <param name="list">Отсортированный список.</param>
            public ValueCollection(SortedLinkedList<TKey, TValue> list)
            {
                source = list;
            }

            void ICollection<TValue>.Add(TValue item)
            {
                throw new NotSupportedException();
            }

            bool ICollection<TValue>.Remove(TValue item)
            {
                throw new NotSupportedException();
            }

            bool ICollection<TValue>.Contains(TValue item)
            {
                throw new NotSupportedException();
            }

            void ICollection<TValue>.Clear()
            {
                throw new NotSupportedException();
            }

            /// <summary>
            /// Возвращает перечислитель исходной коллекции.
            /// </summary>
            /// <returns>Перечислитель.</returns>
            IEnumerator IEnumerable.GetEnumerator()
            {
                return GetEnumerator();
            }

            /// <summary>
            /// Возвращает перечислитель исходной коллекции.
            /// </summary>
            /// <returns>Перечислитель.</returns>
            IEnumerator<TValue> IEnumerable<TValue>.GetEnumerator()
            {
                return GetEnumerator();
            }

            /// <summary>
            /// Копирует ключи исходной коллекции в массив с указанного индекса.
            /// </summary>
            /// <param name="array">Массив.</param>
            /// <param name="index">Индекс.</param>
            void ICollection.CopyTo(Array array, int index)
            {
                if (ReferenceEquals(array, null))
                    throw new ArgumentNullException(nameof(array));
                if (array.Rank != 1)
                    throw new RankException(nameof(array));
                if ((index < 0) || (index >= array.Length))
                    throw new IndexOutOfRangeException(nameof(index));
                if (array.Length - index < Count)
                    throw new ArgumentException(nameof(index));
                Node<TKey, TValue> current = source.Head;
                while (!ReferenceEquals(current, null))
                {
                    array.SetValue(current.Pair.Value, index++);
                    current = current.Next;
                }
            }

            /// <summary>
            /// Копирует ключи исходной коллекции в массив с указанного индекса.
            /// </summary>
            /// <param name="array">Массив.</param>
            /// <param name="index">Индекс.</param>
            public void CopyTo(TValue[] array, int index)
            {
                if (ReferenceEquals(array, null))
                    throw new ArgumentNullException(nameof(array));
                if ((index < 0) || (index >= array.Length))
                    throw new IndexOutOfRangeException(nameof(index));
                if (array.Length - index < Count)
                    throw new ArgumentException(nameof(index));
                Node<TKey, TValue> current = source.Head;
                while (!ReferenceEquals(current, null))
                {
                    array[index++] = current.Pair.Value;
                    current = current.Next;
                }
            }

            /// <summary>
            /// Возвращает перечислитель исходной коллекции.
            /// </summary>
            /// <returns>Перечислитель.</returns>
            public Enumerator GetEnumerator()
            {
                return new Enumerator(this);
            }

            /// <summary>
            /// Перечислитель.
            /// </summary>
            public struct Enumerator : IEnumerator<TValue>
            {
                private readonly int version;

                private readonly ValueCollection values;

                private int index;

                private Node<TKey, TValue> current;

                public void ThrowWhenCollectionWasChanged()
                {
                    if (version != values.source.version)
                        throw new InvalidOperationException("Коллекция была изменена");
                }

                /// <summary>
                /// Текущее значение.
                /// </summary>
                object IEnumerator.Current
                {
                    get
                    {
                        ThrowWhenCollectionWasChanged();
                        return Current;
                    }
                }

                /// <summary>
                /// Текущее значение.
                /// </summary>
                public TValue Current => current.Pair.Value;

                /// <summary>
                /// Конструирует перечислитель на основе коллекции значений.
                /// </summary>
                /// <param name="values">Коллекция значения.</param>
                public Enumerator(ValueCollection values) : this()
                {
                    version = values.source.version;
                    this.values = values;
                    index = -1;
                }

                /// <summary>
                /// Сдвигает перечислитель на один элемент.
                /// </summary>
                /// <returns>True, если смещение перечислителя возможно в дальнейшем, иначе - False.</returns>
                public bool MoveNext()
                {
                    ThrowWhenCollectionWasChanged();
                    if (index == -1)
                        current = values.source.Head;
                    else
                        current = current.Next;
                    return ++index < values.Count;
                }

                /// <summary>
                /// Переустанавливает перечислитель в начальную позицию.
                /// </summary>
                public void Reset()
                {
                    ThrowWhenCollectionWasChanged();
                    index = -1;
                }

                /// <summary>
                /// Высвобождает неуправляемые ресурсы, используемые объектом.
                /// </summary>
                public void Dispose()
                {
                }
            }
        }

        /// <summary>
        /// Коллекция ключей.
        /// </summary>
        [DebuggerTypeProxy(typeof(CollectionDebugView))]
        [DebuggerDisplay("Count = {Count}")]
        [Serializable]
        public class KeyCollection : ICollection<TKey>, ICollection
        {
            private readonly SortedLinkedList<TKey, TValue> source;

            /// <summary>
            /// Длина коллекции.
            /// </summary>
            public int Count => source.Count;

            /// <summary>
            /// Объект синхронизации.
            /// </summary>
            public object SyncRoot => source.SyncRoot;

            /// <summary>
            /// Указывает является ли исходная коллекция синхронизированной.
            /// </summary>
            public bool IsSynchronized => source.IsSynchronized;

            /// <summary>
            /// Указывает является ли коллекция коллекцией только на чтение.
            /// </summary>
            public bool IsReadOnly => true;

            /// <summary>
            /// Конструирует коллекцию значений из отсортированного списка.
            /// </summary>
            /// <param name="list">Отсортированный список.</param>
            public KeyCollection(SortedLinkedList<TKey, TValue> list)
            {
                source = list;
            }

            void ICollection<TKey>.Add(TKey item)
            {
                throw new NotSupportedException();
            }

            bool ICollection<TKey>.Remove(TKey item)
            {
                throw new NotSupportedException();
            }

            bool ICollection<TKey>.Contains(TKey item)
            {
                throw new NotSupportedException();
            }

            void ICollection<TKey>.Clear()
            {
                throw new NotSupportedException();
            }

            /// <summary>
            /// Возвращает перечислитель исходной коллекции.
            /// </summary>
            /// <returns>Перечислитель.</returns>
            IEnumerator IEnumerable.GetEnumerator()
            {
                return GetEnumerator();
            }

            /// <summary>
            /// Возвращает перечислитель исходной коллекции.
            /// </summary>
            /// <returns>Перечислитель.</returns>
            IEnumerator<TKey> IEnumerable<TKey>.GetEnumerator()
            {
                return GetEnumerator();
            }

            /// <summary>
            /// Копирует ключи исходной коллекции в массив с указанного индекса.
            /// </summary>
            /// <param name="array">Массив.</param>
            /// <param name="index">Индекс.</param>
            void ICollection.CopyTo(Array array, int index)
            {
                if (ReferenceEquals(array, null))
                    throw new ArgumentNullException(nameof(array));
                if (array.Rank != 1)
                    throw new RankException(nameof(array));
                if ((index < 0) || (index >= array.Length))
                    throw new IndexOutOfRangeException(nameof(index));
                if (array.Length - index < Count)
                    throw new ArgumentException(nameof(index));
                Node<TKey, TValue> current = source.Head;
                while (!ReferenceEquals(current, null))
                {
                    array.SetValue(current.Pair.Key, index++);
                    current = current.Next;
                }
            }

            /// <summary>
            /// Копирует ключи исходной коллекции в массив с указанного индекса.
            /// </summary>
            /// <param name="array">Массив.</param>
            /// <param name="index">Индекс.</param>
            public void CopyTo(TKey[] array, int index)
            {
                if (ReferenceEquals(array, null))
                    throw new ArgumentNullException(nameof(array));
                if ((index < 0) || (index >= array.Length))
                    throw new IndexOutOfRangeException(nameof(index));
                if (array.Length - index < Count)
                    throw new ArgumentException(nameof(index));
                Node<TKey, TValue> current = source.Head;
                while (!ReferenceEquals(current, null))
                {
                    array[index++] = current.Pair.Key;
                    current = current.Next;
                }
            }

            /// <summary>
            /// Возвращает перечислитель исходной коллекции.
            /// </summary>
            /// <returns>Перечислитель.</returns>
            public Enumerator GetEnumerator()
            {
                return new Enumerator(this);
            }

            /// <summary>
            /// Перечислитель.
            /// </summary>
            public struct Enumerator : IEnumerator<TKey>
            {
                private readonly int version;

                private readonly KeyCollection keys;

                private int index;

                private Node<TKey, TValue> current;

                public void ThrowWhenCollectionWasChanged()
                {
                    if (version != keys.source.version)
                        throw new InvalidOperationException("Коллекция была изменена");
                }

                /// <summary>
                /// Текущий ключ.
                /// </summary>
                object IEnumerator.Current
                {
                    get
                    {
                        ThrowWhenCollectionWasChanged();
                        return Current;
                    }
                }

                /// <summary>
                /// Текущий ключ.
                /// </summary>
                public TKey Current => current.Pair.Key;

                /// <summary>
                /// Конструирует перечислитель на основе коллекции ключей.
                /// </summary>
                /// <param name="keys">Коллекция ключей.</param>
                public Enumerator(KeyCollection keys) : this()
                {
                    version = keys.source.version;
                    this.keys = keys;
                    index = -1;
                }

                /// <summary>
                /// Сдвигает перечислитель на один элемент.
                /// </summary>
                /// <returns>True, если смещение перечислителя возможно в дальнейшем, иначе - False.</returns>
                public bool MoveNext()
                {
                    ThrowWhenCollectionWasChanged();
                    if (index == -1)
                        current = keys.source.Head;
                    else
                        current = current.Next;
                    return ++index < keys.Count;
                }

                /// <summary>
                /// Переустанавливает перечислитель в начальную позицию.
                /// </summary>
                public void Reset()
                {
                    ThrowWhenCollectionWasChanged();
                    index = -1;
                }

                /// <summary>
                /// Высвобождает неуправляемые ресурсы, используемые объектом.
                /// </summary>
                public void Dispose()
                {
                }
            }
        }

        /// <summary>
        /// Перечислитель.
        /// </summary>
        public struct Enumerator : IEnumerator<KeyValuePair<TKey, TValue>>, IDictionaryEnumerator
        {
            private readonly int version;

            private readonly SortedLinkedList<TKey, TValue> source;

            private int index;

            private Node<TKey, TValue> current;

            public void ThrowWhenCollectionWasChanged()
            {
                if (version != source.version)
                    throw new InvalidOperationException("Коллекция была изменена");
            }

            /// <summary>
            /// Текущий элемент.
            /// </summary>
            object IEnumerator.Current
            {
                get
                {
                    ThrowWhenCollectionWasChanged();
                    return Current;
                }
            }

            /// <summary>
            /// Ключ.
            /// </summary>
            object IDictionaryEnumerator.Key
            {
                get
                {
                    ThrowWhenCollectionWasChanged();
                    return Key;
                }
            }

            /// <summary>
            /// Значение.
            /// </summary>
            object IDictionaryEnumerator.Value
            {
                get
                {
                    ThrowWhenCollectionWasChanged();
                    return Value;
                }
            }

            /// <summary>
            /// Текущий элемент.
            /// </summary>
            DictionaryEntry IDictionaryEnumerator.Entry
            {
                get
                {
                    ThrowWhenCollectionWasChanged();
                    return new DictionaryEntry(current.Pair.Key, current.Pair.Value);
                }
            }

            /// <summary>
            /// Текущий элемент.
            /// </summary>
            public KeyValuePair<TKey, TValue> Current => new KeyValuePair<TKey, TValue>(current.Pair.Key, current.Pair.Value);

            /// <summary>
            /// Ключ.
            /// </summary>
            public TKey Key => current.Pair.Key;

            /// <summary>
            /// Значение.
            /// </summary>
            public TValue Value => current.Pair.Value;

            /// <summary>
            /// Конструирует перечислитель из указанного отсортированного списка.
            /// </summary>
            /// <param name="list">Отсортированный список.</param>
            public Enumerator(SortedLinkedList<TKey, TValue> list) : this()
            {
                version = list.version;
                source = list;
                index = -1;
            }

            /// <summary>
            /// Сдвигает перечислитель на один элемент.
            /// </summary>
            /// <returns>True, если смещение перечислителя возможно в дальнейшем, иначе - False.</returns>
            public bool MoveNext()
            {
                ThrowWhenCollectionWasChanged();
                if (index == -1)
                    current = source.Head;
                else
                    current = current.Next;
                return ++index < source.Count;
            }

            /// <summary>
            /// Переустанавливает перечислитель в начальную позицию.
            /// </summary>
            public void Reset()
            {
                ThrowWhenCollectionWasChanged();
                index = -1;
            }

            /// <summary>
            /// Высвобождает неуправляемые ресурсы, используемые объектом.
            /// </summary>
            public void Dispose()
            {
            }
        }

        object IDictionary.this[object key]
        {
            get => this[(TKey)key];
            set
            {
                this[(TKey)key] = (TValue)value;
                version++;
            }
        }

        public TValue this[TKey key]
        {
            get
            {
                if (ReferenceEquals(key, null))
                    throw new ArgumentNullException(nameof(key));
                return Find(key).Pair.Value;
            }
            set
            {
                if (ReferenceEquals(key, null))
                    throw new ArgumentNullException(nameof(key));
                Node<TKey, TValue> target = Find(key);
                target.Pair = new KeyValuePair<TKey, TValue>(key, value);
                version++;
            }
        }

        /// <summary>
        /// Список значений.
        /// </summary>
        ICollection<TValue> IDictionary<TKey, TValue>.Values => Values;

        /// <summary>
        /// Список ключей.
        /// </summary>
        ICollection<TKey> IDictionary<TKey, TValue>.Keys => Keys;

        /// <summary>
        /// Список значений.
        /// </summary>
        ICollection IDictionary.Values => Values;

        /// <summary>
        /// Список ключей.
        /// </summary>
        ICollection IDictionary.Keys => Keys;

        /// <summary>
        /// Список значений.
        /// </summary>
        IEnumerable<TValue> IReadOnlyDictionary<TKey, TValue>.Values => Values;

        /// <summary>
        /// Список ключей.
        /// </summary>
        IEnumerable<TKey> IReadOnlyDictionary<TKey, TValue>.Keys => Keys;

        /// <summary>
        /// Список значений.
        /// </summary>
        public ValueCollection Values
        {
            get
            {
                if (ReferenceEquals(values, null))
                    values = new ValueCollection(this);
                return values;
            }
        }

        /// <summary>
        /// Список ключей.
        /// </summary>
        public KeyCollection Keys
        {
            get
            {
                if (ReferenceEquals(keys, null))
                    keys = new KeyCollection(this);
                return keys;
            }
        }

        /// <summary>
        /// Длина списка.
        /// </summary>
        public int Count { get; private set; }

        /// <summary>
        /// Объект синхронизации.
        /// </summary>
        public object SyncRoot { get; } = new object();

        /// <summary>
        /// Указывает синхронизирована ли коллекция.
        /// </summary>
        public bool IsSynchronized => false;

        /// <summary>
        /// Указывает является коллекция коллекцией только для чтения.
        /// </summary>
        public bool IsReadOnly => false;

        /// <summary>
        /// Указывает является ли коллекция коллекцией с фиксированным размером.
        /// </summary>
        public bool IsFixedSize => false;

        /// <summary>
        /// Голова списка.
        /// </summary>
        public Node<TKey, TValue> Head { get; private set; }

        /// <summary>
        /// Компаратор ключей.
        /// </summary>
        public IComparer<TKey> KeysComparer { get; private set; }

        /// <summary>
        /// Конструирует отсортированный список на основе словаря и компаратора ключей.
        /// </summary>
        /// <param name="dictionary">Словарь.</param>
        /// <param name="comparer">Компаратор ключей.</param>
        public SortedLinkedList(IDictionary<TKey, TValue> dictionary, IComparer<TKey> comparer)
        {
            if (ReferenceEquals(dictionary, null))
                throw new ArgumentNullException(nameof(dictionary));
            if (ReferenceEquals(comparer, null))
                throw new ArgumentNullException(nameof(comparer));
            Count = dictionary.Count;
            KeysComparer = comparer;
            foreach (var pair in dictionary)
                Add(pair.Key, pair.Value);
        }

        /// <summary>
        /// Конструирует отсортированный список на основе словаря и компаратора ключей по умолчанию.
        /// </summary>
        /// <param name="dictionary">Словарь.</param>
        /// <param name="comparer">Компаратор ключей.</param>
        public SortedLinkedList(IDictionary<TKey, TValue> dictionary) : this(dictionary, Comparer<TKey>.Default)
        {
        }

        /// <summary>
        /// Конструирует отсортированный список на основе коллекции и компаратора ключей.
        /// </summary>
        /// <param name="collection">Коллекция.</param>
        /// <param name="comparer">Компаратор ключей.</param>
        public SortedLinkedList(IEnumerable<KeyValuePair<TKey, TValue>> collection, IComparer<TKey> comparer)
        {
            if (ReferenceEquals(collection, null))
                throw new ArgumentNullException(nameof(collection));
            if (ReferenceEquals(comparer, null))
                throw new ArgumentNullException(nameof(comparer));
            KeysComparer = comparer;
            foreach (var pair in collection)
                Add(pair.Key, pair.Value);
        }

        /// <summary>
        /// Конструирует отсортированный список на основе коллекции и компаратора ключей по умолчанию.
        /// </summary>
        /// <param name="collection">Коллекция.</param>
        /// <param name="comparer">Компаратор ключей.</param>
        public SortedLinkedList(IEnumerable<KeyValuePair<TKey, TValue>> collection) : this(collection, Comparer<TKey>.Default)
        {
        }

        /// <summary>
        /// Конструирует пустой отсортированный список на основе компаратора ключей.
        /// </summary>
        /// <param name="dictionary">Словарь.</param>
        /// <param name="comparer">Компаратор ключей.</param>
        public SortedLinkedList(IComparer<TKey> comparer)
        {
            if (ReferenceEquals(comparer, null))
                throw new ArgumentNullException(nameof(comparer));
            KeysComparer = comparer;
        }

        /// <summary>
        /// Конструирует пустой отсортированный список на основе компаратора ключей по умолчанию.
        /// </summary>
        /// <param name="dictionary">Словарь.</param>
        /// <param name="comparer">Компаратор ключей.</param>
        public SortedLinkedList() : this(Comparer<TKey>.Default)
        {
        }

        /// <summary>
        /// Добавляет пару ключ-значение в коллекцию.
        /// </summary>
        /// <param name="key">Ключ.</param>
        /// <param name="value">Значение.</param>
        void ICollection<KeyValuePair<TKey, TValue>>.Add(KeyValuePair<TKey, TValue> pair)
        {
            Add(pair.Key, pair.Value);
        }

        /// <summary>
        /// Удаляет первый элемент с указанным ключём.
        /// </summary>
        /// <param name="key">Ключ.</param>
        /// <returns>True, если элемент был удалён, иначе - False.</returns>
        bool ICollection<KeyValuePair<TKey, TValue>>.Remove(KeyValuePair<TKey, TValue> pair)
        {
            return Remove(pair.Key);
        }

        /// <summary>
        /// Проверяет входит ли элемент с указанным ключём в коллекцию.
        /// </summary>
        /// <param name="key">Ключ.</param>
        /// <returns>True, если искомый элемент входит в коллекцию, иначе - False.</returns>
        bool ICollection<KeyValuePair<TKey, TValue>>.Contains(KeyValuePair<TKey, TValue> pair)
        {
            return ContainsKey(pair.Key);
        }

        /// <summary>
        /// Возвращает перечислитель коллекции.
        /// </summary>
        /// <returns>Перечислитель.</returns>
        IEnumerator<KeyValuePair<TKey, TValue>> IEnumerable<KeyValuePair<TKey, TValue>>.GetEnumerator()
        {
            return GetEnumerator();
        }

        /// <summary>
        /// Возвращает перечислитель коллекции.
        /// </summary>
        /// <returns>Перечислитель.</returns>
        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        /// <summary>
        /// Возвращает перечислитель коллекции.
        /// </summary>
        /// <returns>Перечислитель.</returns>
        IDictionaryEnumerator IDictionary.GetEnumerator()
        {
            return GetEnumerator();
        }

        /// <summary>
        /// Добавляет пару ключ-значение в коллекцию.
        /// </summary>
        /// <param name="key">Ключ.</param>
        /// <param name="value">Значение.</param>
        void IDictionary.Add(object key, object value)
        {
            Add((TKey)key, (TValue)value);
        }

        /// <summary>
        /// Удаляет первый элемент с указанным ключём.
        /// </summary>
        /// <param name="key">Ключ.</param>
        /// <returns>True, если элемент был удалён, иначе - False.</returns>
        void IDictionary.Remove(object key)
        {
            Remove((TKey)key);
        }

        /// <summary>
        /// Проверяет входит ли элемент с указанным ключём в коллекцию.
        /// </summary>
        /// <param name="key">Ключ.</param>
        /// <returns>True, если искомый элемент входит в коллекцию, иначе - False.</returns>
        bool IDictionary.Contains(object key)
        {
            return ContainsKey((TKey)key);
        }

        /// <summary>
        /// Копирует ключи исходной коллекции в массив с указанного индекса.
        /// </summary>
        /// <param name="array">Массив.</param>
        /// <param name="index">Индекс.</param>
        void ICollection.CopyTo(Array array, int index)
        {
            if (ReferenceEquals(array, null))
                throw new ArgumentNullException(nameof(array));
            if (array.Rank != 1)
                throw new RankException(nameof(array));
            if ((index < 0) || (index >= array.Length))
                throw new IndexOutOfRangeException(nameof(index));
            if (array.Length - index < Count)
                throw new ArgumentException(nameof(index));
            Node<TKey, TValue> current = Head;
            while (!ReferenceEquals(current, null))
            {
                array.SetValue(current.Pair, index++);
                current = current.Next;
            }
        }

        /// <summary>
        /// Возвращает перечислитель коллекции.
        /// </summary>
        /// <returns>Перечислитель.</returns>
        public Enumerator GetEnumerator()
        {
            return new Enumerator(this);
        }

        /// <summary>
        /// Добавляет пару ключ-значение в коллекцию.
        /// </summary>
        /// <param name="key">Ключ.</param>
        /// <param name="value">Значение.</param>
        public void Add(TKey key, TValue value)
        {
            if (ReferenceEquals(key, null))
                throw new ArgumentNullException(nameof(key));
            KeyValuePair<TKey, TValue> pair = new KeyValuePair<TKey, TValue>(key, value);
            if (Count == 0)
                Head = new Node<TKey, TValue>(pair, Head);
            else if (KeysComparer.Compare(key, Head.Pair.Key) <= 0)
            {
                if (KeysComparer.Compare(key, Head.Pair.Key) == 0)
                    throw new ArgumentException(nameof(key));
                Head = new Node<TKey, TValue>(pair, Head);
            }
            else
            {
                Node<TKey, TValue> previous = Head;
                while (!ReferenceEquals(previous.Next, null) && (KeysComparer.Compare(key, previous.Next.Pair.Key) >= 0))
                {
                    if (KeysComparer.Compare(key, previous.Next.Pair.Key) == 0)
                        throw new ArgumentException(nameof(key));
                    previous = previous.Next;
                }
                previous.Next = new Node<TKey, TValue>(pair, previous.Next);
            }
            version++;
            Count++;
        }

        /// <summary>
        /// Удаляет первый элемент с указанным ключём.
        /// </summary>
        /// <param name="key">Ключ.</param>
        /// <returns>True, если элемент был удалён, иначе - False.</returns>
        public bool Remove(TKey key)
        {
            if (Count == 0)
                throw new InvalidOperationException(nameof(Count));
            if (ReferenceEquals(key, null))
                throw new ArgumentNullException(nameof(key));
            if (KeysComparer.Compare(key, Head.Pair.Key) == 0)
                Head = Head.Next;
            else
            {
                Node<TKey, TValue> previous = Head;
                while (!ReferenceEquals(previous.Next, null) && (KeysComparer.Compare(key, previous.Next.Pair.Key) != 0))
                    previous = previous.Next;
                if (ReferenceEquals(previous.Next, null))
                    return false;
                previous.Next = previous.Next.Next;
            }
            version++;
            Count--;
            return true;
        }

        /// <summary>
        /// Проверяет входит ли элемент с указанным ключём в коллекцию.
        /// </summary>
        /// <param name="key">Ключ.</param>
        /// <returns>True, если искомый элемент входит в коллекцию, иначе - False.</returns>
        public bool ContainsKey(TKey key)
        {
            return !ReferenceEquals(Find(key), null);
        }

        /// <summary>
        /// Очищает коллекцию.
        /// </summary>
        public void Clear()
        {
            Head = null;
            Count = 0;
            version++;
        }

        /// <summary>
        /// Копирует ключи исходной коллекции в массив с указанного индекса.
        /// </summary>
        /// <param name="array">Массив.</param>
        /// <param name="index">Индекс.</param>
        public void CopyTo(KeyValuePair<TKey, TValue>[] array, int index)
        {
            if (!ReferenceEquals(array, null))
                throw new ArgumentNullException(nameof(array));
            if ((index < 0) || (index >= array.Length))
                throw new IndexOutOfRangeException(nameof(index));
            if (array.Length - index < Count)
                throw new ArgumentException(nameof(index));
            Node<TKey, TValue> current = Head;
            while (!ReferenceEquals(current, null))
            {
                array[index++] = current.Pair;
                current = current.Next;
            }
        }

        /// <summary>
        /// Возвращает элемент с указанным ключём.
        /// </summary>
        /// <param name="key">Ключ.</param>
        /// <param name="value">Значение.</param>
        /// <returns>True, если элемент с искомым ключём найден, иначе - False.</returns>
        public bool TryGetValue(TKey key, out TValue value)
        {
            if (Count == 0)
                throw new InvalidOperationException(nameof(Count));
            Node<TKey, TValue> node = Find(key);
            value = ReferenceEquals(node, null) ? default : node.Pair.Value;
            return !ReferenceEquals(value, null);
        }
    }
}